package com.digileo.payroll;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class MainTest {
    @Test
    public void payrollForYear() throws Exception {
        String expected =
                "2016,1,29,15\n" +
                "2016,2,29,15\n" +
                "2016,3,31,15\n" +
                "2016,4,29,15\n" +
                "2016,5,31,11\n" +
                "2016,6,30,15\n" +
                "2016,7,29,15\n" +
                "2016,8,31,15\n" +
                "2016,9,30,15\n" +
                "2016,10,31,12\n" +
                "2016,11,30,15\n" +
                "2016,12,30,15";
        assertThat(Main.payrollForYear(2016), is(expected));
    }

    @Test
    public void payrollForYearAtRangeMin() throws Exception {
        String expected =
                "1,1,31,12\n" +
                "1,2,28,15\n" +
                "1,3,31,15\n" +
                "1,4,29,15\n" +
                "1,5,31,11\n" +
                "1,6,30,15\n" +
                "1,7,29,15\n" +
                "1,8,31,15\n" +
                "1,9,30,15\n" +
                "1,10,31,12\n" +
                "1,11,30,15\n" +
                "1,12,30,15";
        assertThat(Main.payrollForYear(1), is(expected));
    }

    @Test
    public void payrollForYearAtRangeMax() throws Exception {
        String expected =
                "9999,1,29,15\n" +
                "9999,2,26,15\n" +
                "9999,3,31,15\n" +
                "9999,4,30,15\n" +
                "9999,5,31,12\n" +
                "9999,6,30,15\n" +
                "9999,7,30,15\n" +
                "9999,8,31,11\n" +
                "9999,9,30,15\n" +
                "9999,10,29,15\n" +
                "9999,11,30,15\n" +
                "9999,12,31,15";
        assertThat(Main.payrollForYear(9999), is(expected));
    }

    @Test
    public void validateArguments() throws Exception {
        assertThat(Main.validateArguments(new String[]{}), is(false));
        assertThat(Main.validateArguments(new String[]{"0"}), is(false));
        assertThat(Main.validateArguments(new String[]{"10000"}), is(false));
        assertThat(Main.validateArguments(new String[]{"2000"}), is(true));
        assertThat(Main.validateArguments(new String[]{"2000", "extra"}), is(true));
    }
}