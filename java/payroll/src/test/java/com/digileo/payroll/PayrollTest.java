package com.digileo.payroll;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class PayrollTest {
    @org.junit.Test
    public void salaryDayOfMonthWhenLastDayIsWorkDay() throws Exception {
        assertThat("Pay day for August 2016 should 31", Payroll.payrollFor(2016, 8).getSalaryDay(), equalTo(31));
    }

    @org.junit.Test
    public void salaryDayOfMonthWhenLastDayIsWeekend() throws Exception {
        assertThat("Pay day for July 2016 should 29", Payroll.payrollFor(2016, 7).getSalaryDay(), equalTo(29));
    }


    @org.junit.Test
    public void bonusDayOfMonthWhen15IsWorkDay() throws Exception {
        assertThat("Bonus day for August 2016 should be 15", Payroll.payrollFor(2016, 8).getBonusDay(), equalTo(15));
    }

    @org.junit.Test
    public void bonusDayOfMonthWhen15IsWeekend() throws Exception {
        assertThat("Bonus day for May 2016 should be 11", Payroll.payrollFor(2016, 5).getBonusDay(), equalTo(11));
    }

    @org.junit.Test
    public void toStringShouldReturnCSV() throws Exception {
        assertThat("Pay day for 2016/8 should 31", new Payroll(2016, 8, 31, 15).toString(), equalTo("2016,8,31,15"));
    }
}
