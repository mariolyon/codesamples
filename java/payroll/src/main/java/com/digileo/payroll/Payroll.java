package com.digileo.payroll;

import java.util.Calendar;

class Payroll {
    private final int year;
    private final int month;
    private final int salaryDay;
    private final int bonusDay;

    static Payroll payrollFor(int year, int month) {
        int payDay = payDayFor(year, month);
        int bonusDay = bonusDayFor(year, month);
        return new Payroll(year, month, payDay, bonusDay);
    }


    private static int payDayFor(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1); // the first day of the following month

        do {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        } while(! isWorkDay(calendar));

        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    private static int bonusDayFor(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 15);

        if (! isWorkDay(calendar)) {
            while (! isWednesday(calendar)) {
                calendar.add(Calendar.DAY_OF_MONTH, -1);
            }
        }

        return calendar.get(Calendar.DAY_OF_MONTH);
    }


    private static boolean isWorkDay(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&
                calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY;
    }

    private static boolean isWednesday(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY;
    }

    Payroll(int year, int month, int salaryDay, int bonusDay) {
        this.year = year;
        this.month = month;
        this.salaryDay = salaryDay;
        this.bonusDay = bonusDay;
    }

    int getSalaryDay() {
        return salaryDay;
    }

    int getBonusDay() {
        return bonusDay;
    }

    @Override
    public String toString() {
        return String.format("%d,%d,%d,%d", year, month, salaryDay, bonusDay);
    }
}
