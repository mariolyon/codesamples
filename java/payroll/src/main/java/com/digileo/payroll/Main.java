package com.digileo.payroll;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    private static final String USAGE = "Please supply a year in range 1..9999";
    private static final String HEADER = "Year,Month,SalaryDay,BonusDay";

    public static void main(String[] args) {
        if (validateArguments(args)) {
            int year = Integer.parseInt(args[0]);
            String result = payrollForYear(year);
            System.console().printf("%s\n%s\n", HEADER, result);
        } else {
            showUsage();
        }
    }

    static boolean validateArguments(String[] args) {
        if (args.length == 0) {
            return false;
        }

        try {
            int year = Integer.parseInt(args[0]);
            if (year < 1 || year > 9999) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    private static void showUsage() {
        System.out.println(USAGE);
    }

    static String payrollForYear(int year) {
        IntStream months = IntStream.rangeClosed(1, 12);
        Stream<Payroll> payrollStream = months.mapToObj(m -> Payroll.payrollFor(year, m));
        return payrollStream.map(Payroll::toString).collect(Collectors.joining("\n"));
    }
}
