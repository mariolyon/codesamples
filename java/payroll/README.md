Payroll
=======

## Copyright
Mario Lyon Bouhaidar

## Requirements
Create a command line utility (written in java) which works out the dates that this company needs to pay their employees:
- The staff get paid a basic salary and a bonus each month
- The base salary is paid on the last working day of the month (Mon-Fri)
- On the 15th of each month, the workers are paid their bonuses (unless that day is a Saturday or Sunday, 
 in which case it is paid on the first Wednesday before the 15th)
- This should calculate for a given year (input) and create a CSV as output

## Implementation
- writen in Java 8 
- build tool is Gradle
- accepted year range is 1 to 9999  (because the min and max of this range have been verified)

## Building
- to compile and build the jar file, run command: gradle clean build

## Tests
to run the tests, run command: gradle test

## Running
to run the program, run the following command:
java -jar build/libs/payroll.jar YEAR

where YEAR is a number between 1 and 9999 inclusive

eg: java -jar build/libs/payroll.jar 2017 

produces the following output:
Year,Month,SalaryDay,BonusDay
2017,1,31,11
2017,2,28,15
2017,3,31,15
2017,4,28,12
2017,5,31,15
2017,6,30,15
2017,7,31,12
2017,8,31,15
2017,9,29,15
2017,10,31,11
2017,11,30,15
2017,12,29,15

