Painter
=======

# Copyright
Mario Lyon Bouhaidar

## Task
A simple console version of a drawing program.

The program should work as follows:
1. create a new canvas
2. start drawing on the canvas by issuing various commands
3. quit

The program should support the following commands:
- C w h Should create a new canvas of width w and height h.
- L x1 y1 x2 y2 Should create a new line from (x1,y1) to (x2,y2).
 Only horizontal or vertical lines are supported (So x1 == x2 or y1 == y2).
 Horizontal and vertical lines will be drawn using the 'x' character.

- R x1 y1 x2 y2 Should create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.
- B x y c Should fill the entire area connected to (x,y) with "colour" c. The behaviour of this is the same as that of the "bucket fill" tool in paint programs.
- Q Should quit the program.


## Implementation
Written using Scala 2.11.7

## PreRequisites:
install scala >= 2.11, and sbt

## Unit Tests:
run the unit tests using the command:
sbt test

## Usage:
- run the program using sbt:
sbt run
- Once the sbt console is running, issue commands such as the following:
C 20 20
R 3 3 18 18
L 5 2 5 19
L 1 10 20 10
B 6 15 *
B 4 8 +
Q

- You should end up with the following canvas:
----------------------
|                    |
|    x               |
|  xxxxxxxxxxxxxxxx  |
|  x+x            x  |
|  x+x            x  |
|  x+x            x  |
|  x+x            x  |
|  x+x            x  |
|  x+x            x  |
|xxxxxxxxxxxxxxxxxxxx|
|  x x************x  |
|  x x************x  |
|  x x************x  |
|  x x************x  |
|  x x************x  |
|  x x************x  |
|  x x************x  |
|  xxxxxxxxxxxxxxxx  |
|    x               |
|                    |
----------------------
