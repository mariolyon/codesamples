package painter.tools

import painter.Canvas
import org.specs2.mutable._

class RectangleToolTest extends Specification {

  "RectangleTool" should {
    "not change canvas if the coordinates are out of bounds" in {
      val canvas = Canvas(5, 5)
      val canvasCopy = canvas.copy()

      RectangleTool.draw(0, 0, 3, 3, canvasCopy)
      canvas.toString() must beEqualTo(canvasCopy.toString())
    }
    "should change canvas if the coordinates are correct" in {
      val canvas = Canvas(5, 5)
      RectangleTool.draw(1, 1, 3, 3, canvas)

      val expected =
"""-------
|xxx  |
|x x  |
|xxx  |
|     |
|     |
-------"""
      canvas.toString() must beEqualTo(expected)
    }
  }
}