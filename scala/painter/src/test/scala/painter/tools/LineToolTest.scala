package painter.tools

import painter.Canvas
import org.specs2.mutable._

class LineToolTest extends Specification {

  "LineTool" should {
    "not change canvas if line diagonal " in {
      val canvas = Canvas(30, 30)
      val canvasCopy = canvas.copy()

      LineTool.draw(10, 10, 15, 15, canvasCopy)
      canvas.toString() must beEqualTo(canvasCopy.toString())
    }
    "not change canvas if the coordinates are out of bounds" in {
      val canvas = Canvas(5, 5)
      val canvasCopy = canvas.copy()

      LineTool.draw(1, 1, 1, 10, canvas)
      canvas.toString() must beEqualTo(canvasCopy.toString())

      LineTool.draw(10, 10, 10, 1, canvasCopy)
      canvas.toString() must beEqualTo(canvasCopy.toString())
    }
    "not change canvas if the coordinates are in reverse order" in {
      val canvas = Canvas(5, 5)
      val canvasCopy = canvas.copy()

      LineTool.draw(5, 2, 1, 2, canvas)
      canvas.toString() must beEqualTo(canvasCopy.toString())
    }
    "should change canvas if the coordinates are correct" in {
      val canvas = Canvas(5, 5)
      LineTool.draw(1, 1, 1, 3, canvas)

      val expected =
"""-------
|x    |
|x    |
|x    |
|     |
|     |
-------"""
      canvas.toString() must beEqualTo(expected)
    }
  }
}