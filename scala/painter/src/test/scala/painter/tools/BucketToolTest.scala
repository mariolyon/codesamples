package painter.tools

import painter.Canvas
import org.specs2.mutable._

class BucketToolTest extends Specification {

  "BucketTool" should {
    "not change canvas if the coordinates are out of bounds" in {
      val canvas = Canvas(5, 5)
      val canvasCopy = canvas.copy()

      BucketTool.draw(0, 0, 'f', canvasCopy)
      canvas.toString() must beEqualTo(canvasCopy.toString())
    }
    "should change canvas if the coordinates are correct" in {
      val canvas = Canvas(5, 5)
      BucketTool.draw(1, 1, 'f', canvas)

      val expected =
"""-------
|fffff|
|fffff|
|fffff|
|fffff|
|fffff|
-------"""

      canvas.toString() must beEqualTo(expected)
    }
  }
}