package painter

import org.specs2._
import org.specs2.matcher.MustThrownMatchers
import painter.Painter

class PainterTest extends Specification with MustThrownMatchers{

  def is = sequential ^ "Painter Functional Tests".title ^ s2"""
   Painter should
   create a 2x2 canvas as expected                            $createEmptyCanvas2x2
   create a 20x4 canvas as expected                           $createEmptyCanvas20x4
   draw horiziontal line                                      $drawHorizontalLine
   draw vertical                                              $drawVerticalLine
   draw rectangle                                             $drawRectangle
   fill an area                                               $fillArea
   fill a second area                                         $fillSecondArea
                                                               """

  def createEmptyCanvas2x2 = {
      app.evaluate("C 2 2")
      app.canvasAsString must equalTo(emptyCanvas2x2)
  }

  def createEmptyCanvas20x4 = {
    app.evaluate("C 20 4")
    app.canvasAsString must equalTo(emptyCanvas20x4)
  }

  def drawHorizontalLine = {
    app.evaluate("L 1 2 6 2")
    app.canvasAsString must equalTo(canvasAfterFirstLineDrawn)
  }

  def drawVerticalLine = {
    app.evaluate("L 6 3 6 4")
    app.canvasAsString must equalTo(canvasAfterVerticalLineDrawn)
  }

  def drawRectangle = {
    app.evaluate("R 16 1 20 3")
    app.canvasAsString must equalTo(canvasAfterRectangleDrawn)
  }

  def fillArea = {
    app.evaluate("B 10 3 o")
    app.canvasAsString must equalTo(canvasAfterFill)
  }

  def fillSecondArea = {
    app.evaluate("L 2 3 2 3")
    app.evaluate("B 1 3 p")
    app.canvasAsString must equalTo(canvasAfterSecondFill)
  }

  val emptyCanvas2x2 =
"""----
|  |
|  |
----"""

  val emptyCanvas20x4 =
"""----------------------
|                    |
|                    |
|                    |
|                    |
----------------------"""

  val canvasAfterFirstLineDrawn =
"""----------------------
|                    |
|xxxxxx              |
|                    |
|                    |
----------------------"""

  val canvasAfterVerticalLineDrawn =
"""----------------------
|                    |
|xxxxxx              |
|     x              |
|     x              |
----------------------"""

  val canvasAfterRectangleDrawn = 
"""----------------------
|               xxxxx|
|xxxxxx         x   x|
|     x         xxxxx|
|     x              |
----------------------"""

  val canvasAfterFill =
"""----------------------
|oooooooooooooooxxxxx|
|xxxxxxooooooooox   x|
|     xoooooooooxxxxx|
|     xoooooooooooooo|
----------------------"""

  val canvasAfterSecondFill =
"""----------------------
|oooooooooooooooxxxxx|
|xxxxxxooooooooox   x|
|pxpppxoooooooooxxxxx|
|pppppxoooooooooooooo|
----------------------"""

  val app = new Painter

}
