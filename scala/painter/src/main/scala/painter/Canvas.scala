package painter

import scala.collection.mutable.ArrayBuffer

class Canvas(val w:Int, val h:Int) {
  val canvas =  Array.ofDim[Pixel](h + 2, w + 2)
  initCanvas

  def initCanvas = {
    def initHorizontalBorders = {
      for (y <- List(0, (h + 1)))
        for (x <- 0 to (w + 1))
          canvas(y)(x) = HorizontalBorder()
    }

    def initVerticalBorders =  {
      for (x <- List(0, (w + 1)))
        for (y <- 1 to h)
          canvas(y)(x) = VerticalBorder()
    }

    def initEmptyCanvas = {
      for (y <- 1 to h )
        for (x <- 1 to w )
          canvas(y)(x) = Blank()
    }

    initHorizontalBorders
    initVerticalBorders
    initEmptyCanvas
  }

  def getPixel(x:Int, y:Int): Pixel = canvas(y)(x)

  def update(x:Int, y:Int, p:Pixel) = {
    canvas(y)(x) = p
  }

  def copy() = {
    val canvasCopy = new Canvas(w, h)
    for (y <- 0 to h + 1 )
      for (x <- 0 to w + 1 )
        canvasCopy(x, y) = getPixel(x, y)
    canvasCopy
  }

  override def toString():String  = {
    var buf = ArrayBuffer[Char]()
    for(row <- canvas) {
      buf ++= row.mkString("")
      buf ++= "\n"
    }
    buf.remove(buf.length - 1)
    buf.mkString("")
  }
}

object Canvas {
  def apply(w:Int, h:Int) = new Canvas(w, h)
}