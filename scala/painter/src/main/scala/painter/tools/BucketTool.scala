package painter.tools

import collection.mutable.Set
import painter.{Fill, Blank, Canvas}

object BucketTool {
  def draw(x:Int, y:Int, c:Char, canvas: Canvas) = {
    val visited = Set[(Int,Int)]()
    fill(x, y, c, canvas, visited)
  }

  def fill(x:Int, y:Int, c:Char, canvas:Canvas, visited:Set[(Int,Int)]) : Set[(Int,Int)] = {
    if(! visited((x,y))) {
      visited += ((x, y))

      val pixel = canvas.getPixel(x, y)

      if( pixel.isInstanceOf[Blank])  {
        canvas(x, y) = Fill(c)
        for ((x1,y1) <- neighbours(x, y)) {
          fill(x1, y1, c, canvas, visited)
        }
      }

    }
    visited
  }

  def neighbours(x:Int, y:Int) = List((x, y+1), (x + 1, y), (x, y-1), (x - 1, y))
}
