package painter.tools

import painter.Canvas
import painter.tools.LineTool

object RectangleTool {
  def draw(x1:Int, y1:Int, x2:Int, y2:Int, canvas: Canvas) = {
    LineTool.draw(x1, y1, x2, y1, canvas)
    LineTool.draw(x2, y1, x2, y2, canvas)
    LineTool.draw(x1, y2, x2, y2, canvas)
    LineTool.draw(x1, y1, x1, y2, canvas)
  }
}