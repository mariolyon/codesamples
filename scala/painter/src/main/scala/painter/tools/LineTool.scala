package painter.tools

import painter.{Line, Canvas}

object LineTool {
  def draw(x1:Int, y1:Int, x2:Int, y2:Int, canvas: Canvas) = {
    if( x1 > 0 && y1 > 0 && x2 <= canvas.w && y2 <= canvas.h ) {
      if(x1 == x2 ) {
        drawVertical(x1, y1, y2, canvas)
      } else if (y1 == y2) {
        drawHorizontal(x1, x2, y1, canvas)
      }
    }
  }

  def drawHorizontal(x1:Int, x2:Int, y:Int, canvas:Canvas)  = {
    for (x <- x1 to x2) {
      canvas(x, y) = Line()
    }
  }

  def drawVertical(x:Int, y1:Int, y2:Int, canvas:Canvas)  = {
    for (y <- y1 to y2) {
      canvas(x, y) = Line()
    }
  }
}
