package painter

import painter.tools.{RectangleTool, LineTool, BucketTool}
import scala.Predef._

class Painter {
  var canvas:Canvas = null

  def evaluate(line:String) = {
    if (line == "Q") Painter.Stop

    val pattern = "(.)? ?(.*)".r
    val pattern(command, args) = line.trim

    command match {
      case "Q" => Painter.Stop
      case "C" => canvasCommand(args)
      case "L" if(canvas!=null) => lineCommand(args)
      case "R" if(canvas!=null) => rectangleCommand(args)
      case "B" if(canvas!=null) => fillCommand(args)
      case _ => ()
    }
  }

  def canvasCommand(args:String) {
    val pattern = "([0-9]+) ([0-9]+)".r
    if(args.matches(pattern.toString())) {
      val pattern(w, h) = args
      canvas = new Canvas(w.toInt, h.toInt)
    }
  }

  def lineCommand(args:String) {
    val pattern = "([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)".r
    if(args.matches(pattern.toString())) {
      val pattern(x1, y1, x2, y2) = args
      LineTool.draw(x1.toInt, y1.toInt, x2.toInt, y2.toInt, canvas)
    }
  }

  def rectangleCommand(args:String) {
    val pattern = "([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)".r
    if(args.matches(pattern.toString())) {
      val pattern(x1, y1, x2, y2) = args
      RectangleTool.draw(x1.toInt, y1.toInt, x2.toInt, y2.toInt, canvas)
    }
  }

  def fillCommand(args:String) {
    val pattern = "([0-9]+) ([0-9]+) (.)".r
    if(args.matches(pattern.toString())) {
      val pattern(x, y, c) = args
      BucketTool.draw(x.toInt, y.toInt, c.charAt(0), canvas)
    }
  }

  def canvasAsString = if(canvas != null) canvas.toString() else ("")
}

object Painter extends App with ConsoleSupport {
  val Stop = 1

  val app = new Painter

  def readAndEvaluate = {
    val line = readLine("enter command:")
    app.evaluate(line)
  }

  while( readAndEvaluate != Stop) {
    printLine(app.canvasAsString)
  }
}

trait ConsoleSupport {
  def readLine(prompt: String):String = {
    Console.readLine(prompt)
  }

  def readLine:String = {
    Console.readLine()
  }

  def printLine(s:String) = {
    Console.println(s)
  }
}
