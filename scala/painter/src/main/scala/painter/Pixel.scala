package painter

abstract class Pixel
case class VerticalBorder() extends Pixel with PixelToString
case class HorizontalBorder() extends Pixel with PixelToString
case class Line() extends Pixel with PixelToString
case class Blank() extends Pixel with PixelToString
case class Fill(value:Char) extends Pixel with PixelToString

trait PixelToString {
  override def toString() = {
    this match {
      case VerticalBorder() => "|"
      case HorizontalBorder() => "-"
      case Line() => "x"
      case Blank() => " "
      case Fill(value) => value.toString
    }
  }
}