package com.digileo.palindrome

import org.scalatest.FlatSpec
import com.digileo.palindrome.Palindrome.Position

class ResultTest extends FlatSpec {
  "replaces" should "be true if it wraps around another result " in {
    assert(Result("abcde", 1, 5).replaces(Result("abcd", 1, 4)))
  }

  it should "be true if the results are equal" in {
    assert(Result("abcde", 1, 5).replaces(Result("abcde", 1,5)))
  }

  it should "be true if the text are equal but the indices are different" in {
    assert(Result("abcde", 1, 5).replaces(Result("abcde", 7,5)))
  }

  it should "be false if it is contained within another result" in {
    assert(! Result("abcde", 1, 5).replaces(Result("abcdef", 1,6)))
  }

  it should "be false if results partially overlap" in {
    assert(! Result("bcd", 2, 3).replaces(Result("abc", 1,3)))
  }

  "toString" should "get the result in the expected format" in {
    assert(Result("superman", Position(1,3)).toString.equals("Text: upe, Index: 1, Length: 3"))
  }
}
