package com.digileo.palindrome

class ResultsTest extends org.scalatest.FlatSpec {
  "Results" should "only keep the larger palindromes" in {
    val results = new Results()
    results.add(Result("bcd", 2,3))
    results.add(Result("abcde", 1,5))
    assert(results.seq == Seq(Result("abcde", 1,5)))
  }

  it should "only have the three longest Results and must appear in the descending order of length" in {
    val results = new Results()
    results.add(Result("abcde", 1,5))
    results.add(Result("bcdefg", 2,6))
    results.add(Result("defghifg", 4,8))
    results.add(Result("cdefghi", 3,7))
    assert(results.seq == Seq(Result("defghifg", 4,8), Result("cdefghi", 3,7), Result("bcdefg", 2,6)))
  }

  it should "only have unique palindromes no matter what position they are in, keeping the one with the smallest index" in {
    val results = new Results()
    results.add(Result("abcde", 1,5))
    results.add(Result("abcde", 7,5))
    assert(results.seq == Seq(Result("abcde", 1,5)))
  }
}
