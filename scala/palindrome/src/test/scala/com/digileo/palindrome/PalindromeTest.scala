package com.digileo.palindrome

import com.digileo.palindrome.Palindrome.Position

class PalindromeTest extends org.scalatest.FlatSpec {
  "findTopPalindromes" should "get list of the descriptions of the top palindromes" in {
    val expected = Seq("Text: hijkllkjih, Index: 23, Length: 10",
      "Text: defggfed, Index: 13, Length: 8",
      "Text: abccba, Index: 5, Length: 6")

    assert(Palindrome.findTopPalindromes("sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop") == expected)
  }

  "palindromes" should "get list of all the palindrome positions in a given string" in {
    assert(Palindrome.palindromes("aabaa") == Seq(Position(0,2), Position(0,5), Position(1,3), Position(3,2)))
  }

  "positions" should "get all possible positions that could be candidates for being a palindrome" in {
    assert(Palindrome.positions("a") == Seq())

    assert(Palindrome.positions("ab") == Seq(Position(0,2)))

    assert(Palindrome.positions("abc") == Seq(Position(0,2), Position(0,3), Position(1,2)))
  }


  it should "be false for strings that are empty" in {
    assert(!Palindrome.isPalindrome("aabaa", Position(1, 0)))
  }

  "isPalindrome" should "be false for strings that are just one character long" in {
    assert(!Palindrome.isPalindrome("a", Position(0, 1)))
  }

  it should "be true if string is a two character palindrome" in {
    assert(Palindrome.isPalindrome("aa", Position(0, 2)))
  }

  it should "be true if string is three character Palindrome" in {
    assert(Palindrome.isPalindrome("aba", Position(0, 3)))
  }

  it should "be false if string is three characters and not a Palindrome" in {
    assert(!Palindrome.isPalindrome("abc", Position(0, 3)))
  }

  it should "be true if longer string is a palindrome that contains a palindrome" in {
    assert(Palindrome.isPalindrome("aabaafgh", Position(0, 5)))
  }
}
