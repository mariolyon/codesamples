package com.digileo.palindrome

class Results() {
  private var set:Set[Result] = Set()

  def seq = set.toSeq

  def add(result:Result) = {
    removeFromCollectionResultsThatCanBeReplacedBy(result)
    addToCollectionIfNoResultCanReplace(result)
    sortResultsAnOnlyKeepTheBiggestThree()
  }

  private def sortResultsAnOnlyKeepTheBiggestThree() {
    val list = set.toList.sortWith(_.length > _.length).take(3)
    set = list.toSet
  }

  private def removeFromCollectionResultsThatCanBeReplacedBy(result:Result) {
    val overlappedResults = set.filter(x => result.replaces(x))

    if (overlappedResults.nonEmpty) {
      set = set -- overlappedResults
    }
  }

  private def addToCollectionIfNoResultCanReplace(result: Result) {
    val overlappingResults = set.filter(x => x.replaces(result))

    if (overlappingResults.isEmpty) {
      set = set + result
    }
  }
}
