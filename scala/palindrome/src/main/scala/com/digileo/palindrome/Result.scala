package com.digileo.palindrome

import com.digileo.palindrome.Palindrome.Position

object Result {
  def apply(string:String, position: Position) = {
    val text = string.substring(position.start, position.start + position.len)
    new Result(text, position.start, position.len)
  }
}

case class Result(text: String, index:Int, length: Int) {
  override def toString: String = {
    s"Text: $text, Index: $index, Length: $length"
  }

  def replaces(result:Result): Boolean = {
    (hasSameTextAs(result) && appearsBefore(result)) || wrapsAround(result)
  }

  private def wrapsAround(result: Result): Boolean = {
    length >= result.length && index <= result.index && index + length >= result.index + result.length
  }

  private def hasSameTextAs(result: Result): Boolean = {
    text.equals(result.text)
  }

  private def appearsBefore(result: Result): Boolean = {
    index < result.index
  }
}
