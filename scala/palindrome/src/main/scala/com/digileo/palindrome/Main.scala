package com.digileo.palindrome

object Main extends App {
  if (args.length == 0) {
    println("Usage: Please supply a string of characters")
  } else {
    val string = args(0).toLowerCase
    println(Palindrome.findTopPalindromes(string) mkString "\n\n")
  }
}
