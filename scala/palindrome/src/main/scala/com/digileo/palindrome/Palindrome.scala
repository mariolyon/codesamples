package com.digileo.palindrome

object Palindrome {
  case class Position(start:Int, len:Int)

  def findTopPalindromes(string:String): Seq[String] = {
    val positions = palindromes(string)

    val results = new Results()
    positions.foreach(position => results.add(Result(string, position)))

    results.seq.map(_.toString)
  }

  def palindromes(string:String) = {
    positions(string).flatMap(x =>
          if(isPalindrome(string, x))
              Some(x)
          else
            None)
  }

  def positions(string: String): Seq[Position]= {
    val length = string.length

    for (start <- 0 until length - 1;
         len <- 2 until length - start + 1) yield {
        Position(start, len)
    }
  }

  def isPalindrome(string:String, position:Position):Boolean = {
      position.len match {
        case len if len <= 1 => false
        case len if len == 2 || len ==3  => firstAndLastCharactersMatch(string, position.start, position.len)
        case _ => firstAndLastCharactersMatch(string, position.start, position.len) && isPalindrome(string, Position(position.start + 1, position.len - 2))
    }
  }

  private def firstAndLastCharactersMatch(string: String, start: Int, length: Int): Boolean = {
    string.charAt(start) == string.charAt(start + length - 1)
  }
}
