Longest Palindrome 
==================

## Copyright
Mario Lyon Bouhaidar

## Task
Command line Scala application that finds the longest palindromes in a supplied character string.
For example aaba, contains two palindromes: aba, and aa.

## Implementation
Written using Scala 2.11.7

## Assumptions:
- A palindrome is at least two characters
- The results are in lowercase, so even if abA is given, the resulting palindrome is aba
- The code is intended to be correct more than performant and has not been tested with very large strings.
- The test string does not contain spaces, otherwise the first word is used

## PreRequisites:
install scala >= 2.11, and sbt 

## Unit Tests:
run the unit tests using the command:
sbt test

## Usage:
sbt "run <string of characters>"
eg: sbt "run abaccddcc"
